from django.urls import path, include
from django.contrib import admin
from . import views

urlpatterns = [
    path('', views.index),
    path('admin/', admin.site.urls),
    # APIs
    path('', include('users.urls')),
    path('', include('children.urls')),

]
