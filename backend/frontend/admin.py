from django.contrib import admin

from users.models import User
from children.models import FoundChild, LostChild

# users
admin.site.register(User)
# children
admin.site.register(FoundChild)
admin.site.register(LostChild)
