from rest_framework import routers

from .api import FoundChildViewSet, LostChildViewSet

# Routers to determine the URL conf
router = routers.DefaultRouter()
router.register('api/children/found', FoundChildViewSet, 'Found Children')
router.register('api/children/lost', LostChildViewSet, 'Lost Children')

urlpatterns = router.urls
