from rest_framework import viewsets, permissions

from .serializers import FoundChildSerializer, LostChildSerializer
from .models import FoundChild, LostChild


# Found Child ViewSet
class FoundChildViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = FoundChild.objects.all()
    serializer_class = FoundChildSerializer


# Lost Child ViewSet
class LostChildViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = LostChild.objects.all()
    serializer_class = LostChildSerializer
