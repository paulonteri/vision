from rest_framework import serializers

from .models import FoundChild, LostChild


# Found Child Serializer
class FoundChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = FoundChild
        fields = '__all__'


# Lost Child Serializer
class LostChildSerializer(serializers.ModelSerializer):
    class Meta:
        model = LostChild
        fields = '__all__'
