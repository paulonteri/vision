from django.db import models
from django.utils import timezone

from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin


# User Manager
class UserManager(BaseUserManager):

    def _create_user(self, email, user_name, password, is_staff, is_superuser, is_admin, **extra_fields):
        if not email:
            raise ValueError('Users must have an email address')
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            email=email,
            user_name=user_name,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            is_admin=is_admin,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, user_name, password, **extra_fields):
        return self._create_user(email, user_name, password, False, False, False, **extra_fields)

    def create_superuser(self, email, user_name, password, **extra_fields):
        user = self._create_user(email, user_name, password, True, True, True, **extra_fields)
        return user


# Custom User
class User(AbstractUser, PermissionsMixin):
    email = models.EmailField(verbose_name="email", max_length=60, unique=True)
    user_name = models.CharField(verbose_name="User name", max_length=20, unique=True)
    first_name = models.CharField(verbose_name="First name", max_length=25, blank=True)
    last_name = models.CharField(verbose_name="Last name", max_length=25, blank=True)
    national_id = models.IntegerField(null=True, blank=True)
    date_joined = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login = models.DateTimeField(verbose_name="last login", auto_now=True)
    email_confirmed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    USERNAME_FIELD = 'email'
    # EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['user_name']

    objects = UserManager()
