from rest_framework import viewsets, permissions

from .models import User
from .serializers import UserSerializer


# User ViewSet
class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [permissions.AllowAny]
    queryset = User.objects.all()
    serializer_class = UserSerializer
