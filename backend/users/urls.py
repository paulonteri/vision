from rest_framework import routers

from .api import UserViewSet

# Router to determine the URL conf
router = routers.DefaultRouter()
router.register('api/users', UserViewSet, 'Users')

urlpatterns = router.urls
